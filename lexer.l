/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

%option noinput
%option nounput
%{
#include <stdio.h>
#include "techtree.h"
#include "y.tab.h"
%}

%%
x				return X;
y				return Y;
alt				return ALT;
border				return BORDER;
class				return CLASS;
css				return CSS;
desc				return DESC;
edge				return EDGE;
height				return HEIGHT;
image				return IMAGE;
link				return LINK;
name				return NAME;
requires			return REQUIRES;
node				return NODE;
title				return TITLE;
top				return TOP;
maxwidth			return MAXWIDTH;
width				return WIDTH;
\+				return PLUS;
\-				return MINUS;
\}				return BRACECLOSE;
\{				return BRACEOPEN;
:				return COLON;
,				return COMMA;
[0-9]+				{ yylval.ival = atoi(yytext); return NUMBER; }
\"[^"\n]*["\n] {
        yylval.sval = strdup(yytext + 1);
        if (yylval.sval[yyleng - 2] != '"')
                fprintf(stderr, "double quoted string not closed\n");
        else
                yylval.sval[yyleng-2] = 0;
        return DBLQUOTEDSTRING;
}
\n                              { ++lineno; /* ignore newline */ }
[ \t]+                          /* ignore whitespace */;
\#[^\n]*                        return COMMENT;
%%
