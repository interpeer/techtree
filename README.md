# techtree

A tool to generate an SVG  node-map, computer-game style technology tree etc. from a
file of nodes and dependencies.

You can see examples [here](https://librecast.net/2019-08-078.html) and
[here](https://librecast.net/techtree.html).

![https://librecast.net/media/2019-08-078.svg](https://librecast.net/media/2019-08-078.svg "NGI0 Librecast LIVE Technology Tree")

## Usage

`techtree [options] < source.tree > output.svg`

NB: when embedding the image on a webpage, in order for the SVG to be interactive (for links to work, and for embedded images to load) you need to display the image using an `<object>` tag, not an `<img>` tag. This is due to "security reasons", and is not just some dodgy hack by browser developers to ensure the codepath for rendering the image uses the "object" code and not the "img" code. No, really.

eg. `<object type="image/svg+xml" class="techtree" data="/media/2019-08-078.svg"></object>`

## Options

All the following options can also be supplied as globals at the top of the
.tree file. Options in the file take precedence over commandline options.

--circle
: draw nodes as circles instead of the default rectangles

--edge
: set minimum length of edge (lines between nodes)

--desc "description"
: set description

--css filename
: CSS stylesheet (will be embedded in resulting SVG)

--title "title"
: set title

--top
: set top of first node (in pixels)

--height
: set height of SVG

--width
: set width of SVG

## Input File Format

`# Lines beginning with a '#' are comments, which are ignored`

### Global Options

Global options should be placed at the beginning of the input file, before any
node definitions.

border
: draw a border around the SVG. This will be a rectangle with CSS class "outer"

css "example.css"
: embed the CSS from example.css into the SVG

title "Librecast LIVE - NGI0 Discovery Tech Tree"
: set a title

desc "NGI0 Discovery Technology Tree for the Librecast LIVE Project"
: set a description

edge 150
: edge - set the default spacing between nodes (horizontal and vertical)

top 300
: top - set top of first node (in pixels)

height 2500
: set height of SVG (affects node spacing)

width 2750
: set width of SVG (affects node spacing)

maxwidth 2500
: set maximum width of SVG (does NOT affect node spacing)

### Nodes

Nodes with no dependencies ("requires") will be plotted first in the order in which they appear. The next level of nodes will consist of nodes whose dependencies are all "unlocked" on a previous level. This continues until all nodes are plotted.

Nodes with dependencies will have an edge (line) plotted back to each of those
dependencies. No sorting or automatic spacing is done to try and prevent edges
from crossing through nodes. To achieve a nice layout, it may be necessary to
change the order of the nodes in the source file for a level, or to adjust the
positioning of nodes using the 'x' and 'y' directives.

A simple node looks like this:

```
node {
        name: "Pottery"
}
```

Nodes require either a "name" or an "image" directive. All other directives are
optional.

name: "name of node"
: the name of the node. This will be displayed on text nodes, and is used with the
"requires" directive to specify parent nodes.

image: "file/url"
: an image to display, instead of text. Can be a local path or URI. Needs to be
findable by your SVG viewer at the time of rendering.

alt: "Alternate Text for Screen Readers etc."
: please test with a screen reader like orca to make sure your SVG is
accessible.

class: "cssclass1 cssclass2"
: CSS classes to apply to this node

link: "https://example.com/"
: the text/image will be xlinked to this URI

desc: "description"
: a description. Presently ignored.

requires: "name1", "name2"
: a comma-separated list of double-quoted names of parent nodes.

height: NUMBER
: set an explicit height for the node

width: NUMBER
: set an explicit width for the node

x: [+/-] NUMBER
: adjust the x coordinate of the node. Either an absolute number, or can be
relative by prepending +/-

y: [+/-] NUMBER
: adjust the y coordinate of the node. Either an absolute number, or can be
relative by prepending +/-

## Limits / Defaults

This was a quick hack. There are some hard-coded limits in `techtree.h`:

- maximum number of levels (128)
- maximum number of nodes (512)
- maximum number of prerequisites for a single node (16)
- default height/width of node (150/150)
- maximum number of chars per node text line (10) (lines longer than this will
  be automatically wrapped
- Node text height (16)

Adjust this to suit your needs.

## Installation

Requires flex + bison.

```
make
make install # as root
```

## License

GPLv2 or (at your option) GPLv3

## Author

Brett Sheffield <bacs@librecast.net>
