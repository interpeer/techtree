/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

#ifndef TECHTREE_H
#define TECHTREE_H 1

#define LEVELS_MAX 128 /* max number of levels */
#define NODES_MAX 512 /* max number of nodes */
#define REQUIRE_MAX 16 /* max number of prerequisites for a single node */
#define NODE_HEIGHT 150 /* pixel height of node box */
#define NODE_WIDTH 150 /* pixel width of node box */
#define EDGE_MIN 100 /* minimum length of edge (vert gap between nodes) */
#define NODE_TEXT_MAX 10 /* max number of chars per node text line */
#define NODE_TEXT_HEIGHT 26

struct node_s {
        char *name;
        char *alt;
        char *class;
        char *link;
        char *image;
        char *desc;
        char *reqs[REQUIRE_MAX];
	int reqn;
	int level;
	int height;
	int width;
	int yoff;
	int xoff;
	int y;
	int x;
};

extern struct node_s *node;
extern char *svgcss;
extern char *svgtitle;
extern char *svgdesc;
extern int lineno;
extern int nodes;
extern int svgh;
extern int svgw;
extern int svgmaxw;
extern int border;
extern int edgelen;
extern int svgtop;

#endif /* TECHTREE_H */
