/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

#include <stdio.h>
#include "techtree.h"
#include "y.tab.h"
#include "lex.h"

enum {
	NODETYPE_SQUARE,
	NODETYPE_CIRCLE,
};

struct node_s *node;
int lineno = 1;
int nodes = 1;
int lvl_max_nodes;
static int level;
static int nodetype;
char *svgcss;
char *svgtitle;
char *svgdesc;
int edgelen = EDGE_MIN;
int svgh;
int svgw;
int svgmaxw;
int border;
int svgtop;

void nodes_free(struct node_s *node, int nodes)
{
	if (nodes < 1) return;
	for (int i = 0; i < nodes; i++) {
		for (int r = 0; r < REQUIRE_MAX; r++) {
			if (node[i].reqs[r] != NULL) {
				free(node[i].reqs[r]);
			}
		}
		free(node[i].name);
		free(node[i].desc);
	}
	free(node);
}

struct node_s *node_find(struct node_s *node, int nodes, char *name)
{
	if (nodes < 1) return NULL;
	for (int i = 0; i < nodes; i++) {
		if (!strcmp(node[i].name, name)) {
			return &node[i];
		}
	}
	return NULL;
}

int nodes_dump(struct node_s *node, int nodes)
{
	int rc = 0;
	if (nodes < 1) return 0;
	for (int i = 0; i < nodes; i++) {
		rc++;
		printf("%i: node('%s')\n", i, node[i].name);
		printf("    desc: '%s'\n", node[i].desc);
		printf("    parents: %i\n", node[i].reqn);
		for (int r = 0; r < node[i].reqn; r++) {
			if (node[i].reqs[r] != NULL) {
				printf("    requires: '%s'\n", node[i].reqs[r]);
			}
		}
		printf("    level: %i\n", node[i].level);
	}
	return rc;
}

int svg_css(FILE *restrict out, char *stylesheet)
{
	FILE *in;
	char buf[BUFSIZ];
	size_t byt;
	in = fopen(stylesheet, "r");
	if (!in) return -1;
	fprintf(out, "<style>\n");
	while ((byt = fread(buf, 1, sizeof buf, in)) > 0) {
		fwrite(buf, byt, 1, out);
	}
	fclose(in);
	fprintf(out, "</style>\n");
	return 0;
}

int svg_header(FILE *restrict stream, int width, int height)
{
	fprintf(stream, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
	fprintf(stream, "<svg version=\"1.1\" baseProfile=\"full\" width=\"%i\" height=\"%i\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" viewBox=\"0 0 %i %i\">\n", width, height, width, height);
	return 0;
}

int svg_footer(FILE *restrict stream)
{
	fprintf(stream, "</svg>\n");
	return 0;
}

void svg_draw_line(FILE *restrict stream, int x1, int y1, int x2, int y2)
{
	fprintf(stream, "<line class=\"link\" x1=\"%i\" y1=\"%i\" x2=\"%i\" y2=\"%i\" />\n",
			x1, y1, x2, y2);
}

/* plot edges back to parents */
void svg_node_plot_parent_edges(FILE *restrict stream, struct node_s *n)
{
	struct node_s *p = NULL;
	for (int r = 0; r < n->reqn; r++) {
		p = node_find(node, nodes, n->reqs[r]);
		if (p) {
			int x1 = p->x + NODE_WIDTH / 2;
			int y1 = p->y + NODE_HEIGHT;
			int x2 = n->x + NODE_WIDTH / 2;
			int y2 = n->y;
			svg_draw_line(stream, x1, y1, x2, y2);
		}
	}
}

/* break text on whitespace, then output words until next would overrun, then
 * wrap
 * Need to group the text and center vertically
 */
static void svg_wrap_text(FILE *restrict stream, char *text, char *class, int y, int x, int max)
{
	size_t len = strlen(text);
	if (len <= NODE_TEXT_MAX) {
		fprintf(stream, "<text class=\"nodelabel\" y=\"%i\" x=\"%i\" >%s</text>\n",
			y, x, text);
		return;
	}
	char *txt = strdup(text);
	char *tok;
	char *bits[6] = {0};
	int z = sizeof bits / sizeof bits[0];
	int i = 0;
	if (!txt) return;
	tok = strtok(txt, " ");
	do {
		bits[i++] = tok;
	}
	while (i < z && (tok = strtok(NULL, " ")));
	y -= NODE_TEXT_HEIGHT * (i-1) / 2;
	for (int ii = 0; ii < i; ii++) {
		fprintf(stream, "<text class=\"nodelabel %s\" y=\"%i\" x=\"%i\" >%s</text>\n",
			class, y, x, bits[ii]);
		y += NODE_TEXT_HEIGHT;
	}
	free(txt);
}

void svg_node_plot(FILE *restrict stream, struct node_s *node, int y, int x)
{
	char *class = (node->class) ? node->class : "";
	int height = (node->height) ? node->height : NODE_HEIGHT;
	int width = (node->width) ? node->width : NODE_WIDTH;
	if (node->y) y = node->y;
	if (node->x) x = node->x;
	y += node->yoff;
	x += node->xoff;
	if (node->class && !strcmp(node->class, "hidden")) return;
	switch (nodetype) {
		case NODETYPE_CIRCLE:
			fprintf(stream, "<circle class=\"node %s\" cy=\"%i\" cx=\"%i\" \
				r=\"%i\" />\n",
				node->class,
				y+ height / 2, x + width / 2, width / 2);
			break;
		default:
			fprintf(stream, "<rect class=\"node %s\" y=\"%i\" x=\"%i\" \
					height=\"%i\" width=\"%i\" />\n",
					class, y, x, height, width);
	}
#if 0
	char *default_link = "https://librecast.net/roadmap.html";
	if (!node->link) node->link = default_link;
#endif
	if (node->link) {
		fprintf(stream, "<a xlink:href= \"%s\" target=\"_top\">\n", node->link);
		if (node->alt)
			fprintf(stream, "<title>%s</title>\n", node->alt);
		else
			fprintf(stream, "<title>%s</title>\n", node->name);
	}
	if (node->name) {
		svg_wrap_text(stream, node->name, class, y + height / 2, x + width / 2, NODE_TEXT_MAX);
	}
	if (node->link) {
		fprintf(stream, "</a>\n");
	}
	node->y = y;
	node->x = x;
	svg_node_plot_parent_edges(stream, node);
}

void svg_img_plot(FILE *restrict stream, struct node_s *node, int y, int x)
{
	if (node->y) y = node->y;
	if (node->x) x = node->x;
	y += node->yoff;
	x += node->xoff;
	fprintf(stream, "<image xlink:href=\"%s\" x=\"%i\" y=\"%i\" height=\"%i\" width=\"%i\" />\n",
			node->image, x, y, node->height, node->width);
}

int nodes_per_level(struct node_s *node, int nodes, int lvl)
{
	int n = 0;
	for (int i = 0; i < nodes; i++) {
		if (node[i].level == lvl) n++;
	}
	return n;
}

void nodes_plot(FILE *restrict stream, struct node_s *node, int nodes)
{
	int width = (svgw) ? svgw : (NODE_WIDTH + edgelen) * lvl_max_nodes;
	int height = (svgh) ? svgh : (NODE_HEIGHT + edgelen) * level + svgtop;
	int y = svgtop + edgelen / 2;
	int x = edgelen / 2;
	svg_header(stream, (svgmaxw) ? svgmaxw : width, height);
	if (svgcss) svg_css(stream, svgcss);
	if (border) fprintf(stream, "<rect class=\"outer\" height=\"100%%\" width=\"100%%\" />\n");
	if (svgtitle) fprintf(stream, "<title>%s</title>\n", svgtitle);
	if (svgdesc) fprintf(stream, "<desc>%s</desc>\n", svgdesc);
	for (int lvl = 0; lvl < level; lvl++) {
		int lvlnodes = nodes_per_level(node, nodes, lvl);
		/* center the nodes on each level */
		x = (width - lvlnodes * (NODE_WIDTH + edgelen)) / 2 + edgelen / 2;
		for (int i = 0; i < nodes; i++) {
			if (node[i].level != lvl) continue;
			if (!node[i].name && node[i].image)
				svg_img_plot(stream, &node[i], y, x);
			else
				svg_node_plot(stream, &node[i], y, x);
			x += edgelen + NODE_WIDTH;
		}
		y += edgelen + NODE_HEIGHT;
	}
	svg_footer(stream);
}

int node_unlock(struct node_s *node)
{
	if (node->level != -1) return 0;
	node->level = level;
	return 1;
}

int nodes_sort(struct node_s *node, int nodes)
{
	int rc = 0;
	if (nodes < 1) return 0;
	for (int i = 0; i < nodes; i++) {
		int locked = 0;
		if (node[i].level != -1) continue; /* node already unlocked */
		if (!node[i].reqn) {
			/* no parents */
			rc += node_unlock(&node[i]);
			continue;
		}
		for (int r = 0; r < node[i].reqn; r++) {
			struct node_s *p = NULL;
			p = node_find(node, nodes, node[i].reqs[r]);
			if (p) {
				/* if parent found but parent.level not set, skip */
				if (p->level >= level || p->level == -1) {
					locked++;
					break;
				}
			}
			else {
				printf("ERROR: parent '%s' not found\n", node[i].reqs[r]);
				exit(EXIT_FAILURE);
			}
		}
		if (!locked) {
			/* all parents unlocked, set level */
			rc += node_unlock(&node[i]);
		}
	}
	return rc;
}

/* return maximum number of nodes on a level */
int nodes_max_level(struct node_s *node, int nodes)
{
	int max = 0;
	for (int lvl = 0; lvl < level; lvl++) {
		for (int i = 0, c = 0; i < nodes; i++) {
			if (node[i].level == lvl) c++;
			if (c > max) max = c;
		}
	}
	return max;
}

int parse_opts(int argc, char *argv[])
{
	(void)argc;
	for (int i = 0; argv[i]; i++) {
		if (!strcmp(argv[i], "--circle")) {
			nodetype = 1;
		}
		else if (!strcmp(argv[i], "--border")) {
			border = 1;
		}
		else if (!strcmp(argv[i], "--edge")) {
			if (i < argc)
				edgelen = atoi(argv[++i]);
			else
				return EXIT_FAILURE;
		}
		else if (!strcmp(argv[i], "--desc")) {
			if (i < argc)
				svgdesc = argv[++i];
			else
				return EXIT_FAILURE;
		}
		else if (!strcmp(argv[i], "--css")) {
			if (i < argc)
				svgcss = argv[++i];
			else
				return EXIT_FAILURE;
		}
		else if (!strcmp(argv[i], "--title")) {
			if (i < argc)
				svgtitle = argv[++i];
			else
				return EXIT_FAILURE;
		}
		else if (!strcmp(argv[i], "--top")) {
			if (i < argc)
				svgtop = atoi(argv[++i]);
			else
				return EXIT_FAILURE;
		}
		else if (!strcmp(argv[i], "--height")) {
			if (i < argc)
				svgh = atoi(argv[++i]);
			else
				return EXIT_FAILURE;
		}
		else if (!strcmp(argv[i], "--width")) {
			if (i < argc)
				svgw = atoi(argv[++i]);
			else
				return EXIT_FAILURE;
		}
	}
	return 0;
}

int main(int argc, char *argv[])
{
	int rc = parse_opts(argc, argv);
	if (rc) exit(rc);
	node = calloc(NODES_MAX, sizeof(struct node_s));
	yyparse();
	yylex_destroy();
	nodes--;

#if 0
	printf("parsed %i lines\n", lineno);
	printf("parsed %i nodes\n", nodes);
#endif

	/* determine level of each node */
	while (nodes_sort(node, nodes)) level++;
	//nodes_dump(node, nodes);
	lvl_max_nodes = nodes_max_level(node, nodes);
	nodes_plot(stdout, node, nodes);
	nodes_free(node, nodes);

	return 0;
}
