/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

%{
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include "techtree.h"
#include "y.tab.h"
void yyerror(const char *str);
int yylex(void);
int yywrap(void);
int reqs = 0;

%}
%union
{
	int ival;
	char *sval;
}
%token <sval> ALT
%token <sval> BORDER
%token <sval> BRACECLOSE
%token <sval> BRACEOPEN
%token <sval> CSS
%token <sval> CLASS
%token <sval> COLON
%token <sval> COMMA
%token <sval> COMMENT
%token <sval> DBLQUOTEDSTRING
%token <sval> DESC
%token <sval> EDGE
%token <sval> HEIGHT
%token <sval> IMAGE
%token <sval> LINK
%token <sval> MINUS
%token <sval> NAME
%token <sval> NODE
%token <ival> NUMBER
%token <sval> PLUS
%token <sval> REQUIRES
%token <sval> TITLE
%token <sval> TOP
%token <sval> MAXWIDTH
%token <sval> WIDTH
%token <sval> X
%token <sval> Y

%%

globals:
        /* empty */
        | globals global
        ;

global:
	COMMENT { /* skip comment */ }
	|
	BORDER
	{
		border = 1;
	}
	|
	CSS DBLQUOTEDSTRING
	{
		svgcss = $2;
	}
	|
	DESC DBLQUOTEDSTRING
	{
		svgdesc = $2;
	}
	|
	EDGE NUMBER
	{
		edgelen = $2;
	}
	|
	HEIGHT NUMBER
	{
		svgh = $2;
	}
	|
	WIDTH NUMBER
	{
		svgw = $2;
	}
	|
	MAXWIDTH NUMBER
	{
		svgmaxw = $2;
	}
	|
	NODE BRACEOPEN keyvals BRACECLOSE
        {
		nodes++;
		reqs = 0;
	}
	|
	TITLE DBLQUOTEDSTRING
	{
		svgtitle = $2;
	}
	|
	TOP NUMBER
	{
		svgtop = $2;
	}
	;

keyvals:
	/* this space intentionally left blank */
	| keyvals keyval
	;

keyval:
	COMMENT { /* skip comment */ }
	|
	NAME COLON DBLQUOTEDSTRING
	{
		node[nodes - 1].name = $3;
		node[nodes - 1].level = -1;
	}
	|
	ALT COLON DBLQUOTEDSTRING
	{
		node[nodes - 1].alt = $3;
	}
	|
	CLASS COLON DBLQUOTEDSTRING
	{
		node[nodes - 1].class = $3;
	}
	|
	IMAGE COLON DBLQUOTEDSTRING
	{
		node[nodes - 1].image = $3;
	}
	|
	LINK COLON DBLQUOTEDSTRING
	{
		node[nodes - 1].link = $3;
	}
	|
	DESC COLON DBLQUOTEDSTRING
	{
		node[nodes - 1].desc = $3;
	}
	|
	REQUIRES COLON prerequisites
	{
	}
	|
	HEIGHT COLON NUMBER
	{
		node[nodes - 1].height = $3;
	}
	|
	WIDTH COLON NUMBER
	{
		node[nodes - 1].width = $3;
	}
	|
	X COLON PLUS NUMBER
	{
		node[nodes - 1].xoff = $4;
	}
	|
	X COLON MINUS NUMBER
	{
		node[nodes - 1].xoff = -$4;
	}
	|
	Y COLON PLUS NUMBER
	{
		node[nodes - 1].yoff = $4;
	}
	|
	Y COLON MINUS NUMBER
	{
		node[nodes - 1].yoff = -$4;
	}
	|
	X COLON NUMBER
	{
		node[nodes - 1].x = $3;
	}
	|
	Y COLON NUMBER
	{
		node[nodes - 1].y = $3;
	}
	;

prerequisites:
	/* empty */
	|
	DBLQUOTEDSTRING COMMA prerequisites
	{
		node[nodes - 1].reqs[reqs++] = $1;
		node[nodes - 1].reqn++;
	}
	|
	DBLQUOTEDSTRING
	{
		node[nodes - 1].reqs[reqs++] = $1;
		node[nodes - 1].reqn++;
	}
	;

%%
void yyerror(const char *str)
{
	fprintf(stderr,"error on line %i: %s\n", lineno, str);
}

int yywrap(void)
{
	return 1;
}
